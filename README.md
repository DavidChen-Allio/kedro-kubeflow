# Kubernetes-Kedro

## Dependency

- pyenv

## Usage

1. install the local python version if not exist

```
pyenv install
```

2. use a virtual env

```
if [ -d "/venv/bin" ]
then
   python -m venv venv
else
   source venv/bin/activate
fi
```

3. install packages

```
pip install -r requirements.txt
```

4. go into the kedro project folder `allio-data-pipeline` and install kedro packages

```
kedro install
```
