# Setup from Zero

## Install Python on Mac

1. Install [brew](https://brew.sh/) on Mac

2. Install pyenv using brew

```
brew install pyenv
```

3. Install xz

```
brew install xz
```

4. add to ~/.zprofile

```
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/shims:$PATH"
eval "$(pyenv init --path)"
```

5. add to ~/.zshrc

```
eval "$(pyenv init -)"
```

6. Install python 3.8.11

```
pyenv install 3.8.11
```

7. set local python version to 3.8.11 in the project folder

```
pyenv local 3.8.11
```

8. install python version set in the project next time if not installed

```
pyenv install
```

9. set virtual environment

```
python -m venv venv
```

10. load virtual environment whenever the project is load

```
if [ -d "/venv/bin" ]
then
   python -m venv venv
else
   source venv/bin/activate
fi
```

11. generate `requirements.txt`

```
pip freeze > requirements.txt
```

12. install packages next time the project is load

```
pip install -r requirements.txt
```

<!-- ## Install Kubeflow Pipelines SDK

1. install Kubeflow pipeline SDK

```
pip install kfp --upgrade
``` -->

## Install Python on Debian/Ubuntu

1. Install pyenv dependencies

```
sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

2. Install pyenv

```
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

3. add to ~/.zshrc

```
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
```

4. Install python 3.8.11

```
pyenv install 3.8.11
```

5. set local python version to 3.8.11 in the project folder

```
pyenv local 3.8.11
```

6. install python version set in the project next time if not installed

```
pyenv install
```

7. set virtual environment

```
python -m venv venv
```

8. load virtual environment whenever the project is load

```
if [ -d "/venv/bin" ]
then
   python -m venv venv
else
   source venv/bin/activate
fi
```

9. generate `requirements.txt`

```
pip freeze > requirements.txt
```

10. install packages next time the project is load

```
pip install -r requirements.txt
```

## Install Kedro

1. install Kedro

```
pip install kedro
```

## Create New Kedro Project

1. create new Kedro project

```
kedro new
```

2. install kedro requirements
   go into the Kedro project folder

```
kedro install
```

    if there's a problem with some versions of the packages, edit the `src/requirements.txt` directly and then rerun the command.

3. install kedro dependencies
   refer to this [doc](https://kedro.readthedocs.io/en/stable/04_kedro_project_setup/01_dependencies.html) for dependency installation
